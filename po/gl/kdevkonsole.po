# translation of kdevkonsole.po to Galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# mvillarino <mvillarino@users.sourceforge.net>, 2008.
# Xosé <xosecalvo@gmail.com>, 2009.
# Marce Villarino <mvillarino@gmail.com>, 2009, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kdevkonsole\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2015-03-12 20:03+0100\n"
"Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kdevkonsoleview.cpp:107
#, kde-format
msgid "Terminal"
msgstr ""

#: kdevkonsoleview.cpp:110 kdevkonsoleviewplugin.cpp:62
#, kde-format
msgctxt "@title:window"
msgid "Terminal"
msgstr ""

#: kdevkonsoleviewplugin.cpp:60
#, kde-format
msgid "Failed to load 'konsolepart' plugin"
msgstr "Non foi posíbel cargar o complemento «konsolepart»."

#~ msgid "Konsole"
#~ msgstr "Konsole"

#, fuzzy
#~| msgid "Konsole"
#~ msgctxt "@title:window"
#~ msgid "Konsole"
#~ msgstr "Konsole"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Xosé"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "proxecto@trasno.net"

#~ msgid ""
#~ "This plugin provides KDevelop with an embedded konsole for quick and easy "
#~ "command line access."
#~ msgstr ""
#~ "Este engadido fornécelle a KDevelop un konsole integrado para dispor de "
#~ "acceso rápido e sinxelo á liña de ordes."

#~ msgid "Embedded Terminal support"
#~ msgstr "Soporte de terminais embebidas"

#~ msgid "<b>Konsole</b><p>This window contains an embedded console.</p>"
#~ msgstr "<b>Konsole</b><p>Esta fiestra contén unha consola incrustada.</p>"
