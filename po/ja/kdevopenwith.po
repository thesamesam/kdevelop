msgid ""
msgstr ""
"Project-Id-Version: kdevopenwith\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-28 00:56+0000\n"
"PO-Revision-Date: 2010-10-20 21:19-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: openwithplugin.cpp:139
#, kde-format
msgctxt "@item:menu"
msgid "Other..."
msgstr ""

#: openwithplugin.cpp:150
#, kde-format
msgctxt "@title:menu"
msgid "Open With"
msgstr ""

#: openwithplugin.cpp:155
#, kde-format
msgctxt "@title:menu"
msgid "Embedded Editors"
msgstr ""

#: openwithplugin.cpp:159
#, kde-format
msgctxt "@title:menu"
msgid "External Applications"
msgstr ""

#: openwithplugin.cpp:166
#, kde-format
msgctxt "@action:inmenu"
msgid "Open"
msgstr ""

#: openwithplugin.cpp:186
#, kde-format
msgctxt "@item:inmenu"
msgid "Default Editor"
msgstr ""

#: openwithplugin.cpp:272
#, kde-format
msgctxt "%1: mime type name, %2: app/part name"
msgid "Do you want to open all '%1' files by default with %2?"
msgstr ""

#: openwithplugin.cpp:274
#, kde-format
msgctxt "@title:window"
msgid "Set as Default?"
msgstr ""

#: openwithplugin.cpp:275
#, kde-format
msgctxt "@action:button"
msgid "Set as Default"
msgstr ""

#: openwithplugin.cpp:276
#, kde-format
msgctxt "@action:button"
msgid "Do Not Set"
msgstr ""
